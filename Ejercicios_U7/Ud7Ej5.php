<?php
$palabra = $_REQUEST['palabra'];
$palabra = strtolower($palabra);
$palabras = array("Alma", "Algo", "Amo", "Aro", "Animo", "Arbol", "Abrir", "Asturias", "Amén", "Alcohol", "Aprendiz", "Aquí", "Atroz", "Arroz", "Aprisa", "Alamo", "Axioma", "Azul");
$posibles = "";

if ($palabra !== "") {
	$longitud = strlen($palabra);
	foreach ($palabras as $pista) {
		if (stristr($palabra, substr($pista, 0, $longitud))) {
			if ($posibles === "") {
				$posibles = $pista;
			}
			else {
				$posibles .= ", $pista";
			}
		}
	}
}

echo $posibles === "" ? "No hay palabras sugeridas" : $posibles;
?>