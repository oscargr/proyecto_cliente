<?php
$accion = $_REQUEST['accion'];

if (!isset($usuarios)) {
	$usuarios = array(
		array("oscarlee","RandomKey7196"),
		array("juanaPG","TestDeArray2"),
		array("pacoP","PruebaKey5")
	);
}

class Usuario
{
	function __construct($usu, $pass)
	{
		$this->usu = $usu;
		$this->pass = $pass;
	}
}

if ($accion === "registro") {
	$user = $_REQUEST['usuario'];
	$pwd = $_REQUEST['pass'];
	array_push($usuarios, new Usuario($user, $pwd));
	echo "<span class='text-success'>Registro con éxito.</span>";
}

elseif ($accion === "acceso") {
	$user = $_REQUEST['usuario'];
	$pwd = $_REQUEST['pass'];
	$existe = false;
	foreach ($usuarios as $usuario) {
		if ($usuario[0] == $user && $usuario[1] == $pwd) {
			$existe = true;
		}
	}
	if ($existe) {
		echo "<span class='text-success'>Acceso con éxito.</span>";
	}
	else {
		echo "<span class='text-danger'>No se ha encontrado el usuario o la contraseña no es correcta.</span>";
	}
}
?>