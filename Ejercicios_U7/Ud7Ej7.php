<?php
$dni = $_REQUEST['nif'];

$dni = strtoupper($dni);

$nifs = array(
	'71907845R' => array("Óscar","Gil","c/ Ría de Avilés"),
	'65478915H' => array("María","García","c/ Avenida de Alemania"),
	'71904562L' => array("Pedro","Plaza","c/ Calle del Rosal"),
	'11111111A' => array("Felipe","Rodríguez","c/ Fernández Balsera"),
	'22222222B' => array("Juana","de Dios","c/ Sin nombre")
);

$existe = false;

foreach ($nifs as $num => $datos) {
	if ($dni === $num) {
		$existe = true;
		$dniCorrecto = $datos;
	}
}

if ($existe) {
	echo "<table class='table table-dark table-stripped table-hover'><thead><tr><th>Nombre</th><th>Apellido</th><th>Dirección</th></tr></thead><tbody><tr>";
	foreach ($dniCorrecto as $dato) {
		echo "<td>$dato</td>";
	}
	echo "</tr></tbody></table>";
}
else {
	echo "No hay datos de ese DNI";
}
?>