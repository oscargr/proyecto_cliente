<?php
$usuario = $_REQUEST['usuario'];
$comentario = $_REQUEST['comentario'];

if (($usuario == "" && $comentario == "") || ($usuario !== "" && $comentario == "")) {
	echo "No se ha enviado ningún comentario";
}

if ($usuario == "" && $comentario !== "") {
	$ca = "Anónimo a ".date('d-m-Y H:i:s')." :\r\n".$comentario."\r\n";
	$txt = fopen("comentarios.txt","a") or die("No se ha podido abrir el archivo");
	fwrite($txt, $ca);
	fclose($txt);
	echo "Comentario guardado con éxito";
}

if ($usuario !== "" && $comentario !== "") {
	$cu = $usuario." a ".date('d-m-Y H:i:s')." :\r\n".$comentario."\r\n";
	$txt = fopen("comentarios.txt","a") or die("No se ha podido abrir el archivo");
	fwrite($txt, $cu);
	fclose($txt);
	echo "Comentario guardado con éxito";
}
?>