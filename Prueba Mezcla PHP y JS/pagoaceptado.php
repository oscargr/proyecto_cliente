<!DOCTYPE html>
<html lang="es">
<head>
	<title>Pago aceptado</title>
	<meta charset="utf-8">
	<meta name="author" content="Óscar Gil Riesgo">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="estilo.css">
</head>
<body>
	<?php

	if (isset($_POST['send'])) {
		$ntarjeta=$_POST['ntarjeta'];

		$ncod=substr_replace($ntarjeta, "************", 0, 12);

	}

	?>

	<div id="main2">
		<h2>Información del pago</h2>
		<p>Su tarjeta con número <strong><?php echo $ncod; ?></strong> ha sido aceptada.<br/><br/>Se procederá al pago.</p>
		<div id="load"></div>
	</div>
</body>
</html>